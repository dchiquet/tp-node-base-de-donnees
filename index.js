const express = require('express');
const axios = require('axios').default;
const app = express();
const mysql = require('mysql');
app.use(express.json());
app.use(express.urlencoded());
const pokemon_url = 'https://pokeapi.co/api/v2';

// const url = 'https://jsonplaceholder.typicode.com'
let connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'daniel',
    password : 'chiquet',
    database : 'express_tp'
});

connection.connect();

app.get('/', function (req, res) {
    res.send('Hello World')
})

//  obtenir la liste de tous les users
app.get(`/users`, function (req, res) {
    connection.query("SELECT * FROM users", function (err, result, fields) {
        if (err) throw err;
        res.send(result);
    });
});



//  obtenir les infos du user :id
app.get(`/users/:id`, function (req, res) {
    connection.query(`SELECT * FROM users where user_id = ${req.params.id}`, function (err, result, fields) {
        if (err) throw err;
        res.send(result);
    });
})

//  obtenir la liste de tous les pokemons de user :id
app.get(`/users/:id/pokemons`, function (req, res) {
    let list =''
    connection.query(`SELECT * FROM pokemon_user_list where user_id = ${req.params.id}`, async function (err, result, fields) {
        if (err) throw err;
        for (const i in result) {
            try {
                let { data } = await axios.get(`${pokemon_url}/pokemon/${result[i].pokemon_id}`);
                list += ('nom: '+(data.name)+' - id: '+(data.id));
                if (i<(result.length-1)) list += '/';
            } catch (error) {
                console.error(error);
            }
        }
        res.send(list);
    });
})

//  crée un user
app.post('/users', function (req,res){
    let sql = "INSERT INTO users (pseudo,email, firstname,lastname,createdAt,updateAt) VALUES ?";
    let date;
    date = new Date().toISOString().slice(0, 19).replace('T', ' ');
    console.log(date)
    let values = [
        [req.body.pseudo, req.body.email, req.body.firstname, req.body.lastname,date,date]
    ];
    connection.query(sql, [values], function (err, result) {
        if (err) throw err;
        res.send("Number of records inserted: " + result.affectedRows);
    });
})

//  ajoute un pokemon au user :id
app.post('/users/:id/pokemons', async function (req,res){
    let sql = "INSERT INTO pokemon_user_list (user_id, pokemon_id) VALUES ?";
    let pokemon_id;

    // cherche l'id en fonction de ce qu'il recoit
    try {
        let { data } = await axios.get(`${pokemon_url}/pokemon/${req.body.pokemon_id}`);
        pokemon_id = data.id;
    } catch (error) {
        console.error(error);
    }

    // s'il n'exist pas on arréte
    if (pokemon_id == null){
        res.send('Pokémon not found');
        return;
    }

    // vérifie s'il est déjà lié à l'utilisateur
    let conStr = "SELECT EXISTS( SELECT 1 FROM `pokemon_user_list` WHERE `pokemon_id` = '"+pokemon_id+"' and user_id = '"+req.params.id+"') as is_exist";
    connection.query(conStr, function(error, result, field) {
        if(error) throw error;
        if (result[0].is_exist){
            res.send('Already entry');
            return;
        }

    });

    let values = [
        [req.params.id, pokemon_id]
    ];
    connection.query(sql, [values], function (err, result) {
        if (err) throw err;
        res.send("Number of records inserted: " + result.affectedRows);
    });
})

//  modifie les infos de user :id
app.patch('/users/:id', function (req,res){
    let date;
    date = new Date().toISOString().slice(0, 19).replace('T', ' ');
    console.log(date)

    let sql = "UPDATE users SET pseudo = '"+req.body.pseudo+"', email = '"+req.body.email+"', firstname = '"+req.body.firstname+"',lastname = '"+req.body.lastname+"', updateAt = '"+date+"' WHERE user_id = "+req.params.id;
    connection.query(sql, function (err, result) {
        if (err) throw err;
        res.send("Number of records inserted: " + result.affectedRows);
    });
})

//  patch pokemon pas pertinent


//  supprime un user et la liste de tous ses pokemons
app.delete('/users/:id/', async function (req,res){
    let res_final;
    connection.query(`DELETE FROM pokemon_user_list WHERE user_id = ${req.params.id}`, function (err, result) {
        if (err) throw err;
        res_final = result.affectedRows;
    });
    connection.query(`DELETE FROM users WHERE user_id = ${req.params.id}`, function (err, result) {
        if (err) throw err;
        res_final = res_final+ result.affectedRows;
    });
    res.send("Number of records deleted: "+ res_final )
})


//  supprime un pokemon de user :id
app.delete('/users/:id/pokemons/:pokemon_id', async function (req,res){
    let pokemon_id;

    // cherche l'id en fonction de ce qu'il recoit
    try {
        let { data } = await axios.get(`${pokemon_url}/pokemon/${req.params.pokemon_id}`);
        pokemon_id = data.id;
    } catch (error) {
        console.error(error);
    }

    // s'il n'existe pas on arréte
    if (pokemon_id == null){
        res.send('Pokémon not found');
        return;
    }

    connection.query(`DELETE FROM pokemon_user_list WHERE user_id = ${req.params.id} AND pokemon_id = ${pokemon_id}`, function (err, result) {
        if (err) throw err;
        res.send("Number of records deleted: " + result.affectedRows);
    });
})

app.listen(3000)
